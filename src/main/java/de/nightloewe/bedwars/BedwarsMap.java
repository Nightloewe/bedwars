package de.nightloewe.bedwars;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import de.maltesermailo.api.minigame.Map;
import de.maltesermailo.api.minigame.PlayerHandle;
import de.maltesermailo.impl.minigame.MapLocation;

public class BedwarsMap implements Map {

	//standard map information
	private String name;
	private String author;
	private MapLocation spectatorSpawn;
	private String path;
	
	//bedwars map information
	private List<BedwarsTeam> teams = new ArrayList<BedwarsTeam>();
	private List<MapLocation> villagers = new ArrayList<MapLocation>();
	private List<MapLocation> bronzeSpawns = new ArrayList<MapLocation>();
	private List<MapLocation> silverSpawns = new ArrayList<MapLocation>();
	private List<MapLocation> goldSpawns = new ArrayList<MapLocation>();

	public String getMapName() {
		return this.name;
	}

	public String getAuthor() {
		return this.author;
	}

	public Location getSpectatorSpawn() {
		return this.spectatorSpawn.get();
	}

	public String getPath() {
		return this.path;
	}
	
	public int getMaxPlayers() {
		int maxplayers = 0;
		for(BedwarsTeam team : this.teams) {
			maxplayers = maxplayers + team.getMaxPlayers();
		}
		return maxplayers;
	}
	
	public List<BedwarsTeam> getTeams() {
		return teams;
	}
	
	public List<MapLocation> getVillagers() {
		return villagers;
	}
	
	public List<MapLocation> getBronzeSpawns() {
		return bronzeSpawns;
	}
	
	public List<MapLocation> getSilverSpawns() {
		return silverSpawns;
	}
	
	public List<MapLocation> getGoldSpawns() {
		return goldSpawns;
	}
	
	public void setMapName(String name) {
		this.name = name;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}
	
	public void setSpectatorSpawn(Location spectatorSpawn) {
		this.spectatorSpawn = new MapLocation(spectatorSpawn);
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public void addTeam(BedwarsTeam team) {
		if(!this.teams.contains(team)) {
			this.teams.add(team);
		}
	}
	
	public void removeTeam(String name) {
		for(BedwarsTeam team : this.teams) {
			if(team.getName().equalsIgnoreCase(name)) {
				this.teams.remove(team);
			}
		}
	}
	
	public void addVillager(Location loc) {
		MapLocation mloc = new MapLocation(loc);
		if(!this.villagers.contains(mloc)) {
			this.villagers.add(mloc);
		}
	}
	
	public void removeVillager(Location loc) {
		this.villagers.remove(new MapLocation(loc));
	}
	
	public void addBronzeSpawn(Location loc) {
		MapLocation mloc = new MapLocation(loc);
		if(!this.bronzeSpawns.contains(mloc)) {
			this.bronzeSpawns.add(mloc);
		}
	}
	
	public void removeBronzeSpawn(Location loc) {
		this.bronzeSpawns.remove(new MapLocation(loc));
	}
	
	public void addSilverSpawn(Location loc) {
		MapLocation mloc = new MapLocation(loc);
		if(!this.silverSpawns.contains(mloc)) {
			this.silverSpawns.add(mloc);
		}
	}
	
	public void removeSilverSpawn(Location loc) {
		this.silverSpawns.remove(new MapLocation(loc));
	}
	
	public void addGoldSpawn(Location loc) {
		MapLocation mloc = new MapLocation(loc);
		if(!this.goldSpawns.contains(mloc)) {
			this.goldSpawns.add(mloc);
		}
	}
	
	public void removeGoldSpawn(Location loc) {
		this.goldSpawns.remove(new MapLocation(loc));
	}
	
	public HashMap<BedwarsTeam, Integer> getTeamSizes() {
		HashMap<BedwarsTeam, Integer> teamSizes = new HashMap<BedwarsTeam, Integer>();
		
		for(BedwarsTeam team : this.getTeams()) {
			teamSizes.put(team, team.getTeamSize());
		}
		
		return teamSizes;
	}
	
	public List<PlayerHandle> getPlayersWithoutTeam() {
		List<PlayerHandle> list = new ArrayList<PlayerHandle>();
		
		for(Player p : Bukkit.getOnlinePlayers()) {
			PlayerHandle handle = Bedwars.instance().context().miniGameController().getPlayerHandle(p);
			BedwarsPlayerData data = handle.getPlayerData();
			
			if(data.getTeam() == null) {
				list.add(handle);
			}
		}
		
		return list;
	}
	
	public BedwarsTeam getRandomTeam() {
		int maxTeams = this.getTeams().size();
		
		Random random = new Random();
		
		BedwarsTeam team = this.getTeams().get(random.nextInt(maxTeams));
		
		int teamSize = this.getTeamSizes().get(team);
		
		int attempts = 0;
		while(teamSize >= team.getMaxPlayers()) {
			team = this.getTeams().get(random.nextInt());
			teamSize = this.getTeamSizes().get(team);
			attempts++;
			
			if(attempts == 1000) {
				return null;
			}
		}
		
		return team;
	}
	
	public boolean equalBedLocation(Location block, Location bedLocation) {
		if(block.distance(bedLocation) <= 1) {
			return true;
		}
		return false;
	}
}
