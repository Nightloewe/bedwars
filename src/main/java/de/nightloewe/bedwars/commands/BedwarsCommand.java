package de.nightloewe.bedwars.commands;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.nightloewe.bedwars.Bedwars;
import de.nightloewe.bedwars.BedwarsMap;
import de.nightloewe.bedwars.BedwarsTeam;

public class BedwarsCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if(cs.hasPermission("bedwars.admin")) {
			if(cs instanceof Player) {
				Player p = (Player) cs;
				if(args.length > 0) {
					if(args[0].equalsIgnoreCase("help")) {
						this.sendCommandHelp(p);
					} else if(args[0].equalsIgnoreCase("createmap")) {
						if(args.length != 3) {
							p.sendMessage("/bedwars createmap <worldname> <mapname>");
							return false;
						}
						
						BedwarsMap map = new BedwarsMap();
						map.setPath(new File(Bedwars.instance().context().mapFactory().getMapDirectory(), args[1]).getAbsolutePath());
						
						Bukkit.broadcastMessage(map.getPath());
						
						map.setMapName(args[2]);
						
						Bedwars.instance().context().mapFactory().loadMap(map);
						Bedwars.instance().context().mapFactory().setCurrentMap(map);
						
						World world = Bukkit.getWorld(args[1]);
						
						if(world == null) {
							p.sendMessage(ChatColor.RED + "World couldn't be found, please rename your world directory like your world's name");
							for(World w : Bukkit.getWorlds()) {
								Bukkit.broadcastMessage(w.getName());
							}
							return false;
						}
						
						p.teleport(world.getSpawnLocation());
						p.sendMessage(ChatColor.DARK_GREEN + "World " + args[1] + " successfully loaded and created map " + map.getMapName());
					} else if (args[0].equalsIgnoreCase("savemap")) {
						BedwarsMap map = (BedwarsMap) Bedwars.instance().context().mapFactory().current();
						
						Bedwars.instance().context().mapFactory().saveMap(map);
					} else if(args[0].equalsIgnoreCase("map")) {
						BedwarsMap map = (BedwarsMap) Bedwars.instance().context().mapFactory().current();
						if(args.length > 1) {
							if(args[1].equalsIgnoreCase("author")) {
								if(args.length != 3) {
									p.sendMessage("Autor: " + map.getAuthor());
									return true;
								}
								String author = args[2];
								map.setAuthor(author);
								p.sendMessage(ChatColor.GREEN + "Set map author to " + author);
							} else if(args[1].equalsIgnoreCase("spectatorSpawn")) {
								Location loc = p.getLocation();
								map.setSpectatorSpawn(loc);
								
								p.sendMessage(ChatColor.GREEN + "Set map spectator spawn to current location.");
							} else if(args[1].equalsIgnoreCase("addvillager")) {
								Location loc = p.getLocation();
								map.addVillager(loc);
								
								p.sendMessage(ChatColor.GREEN + "Added villager");
							} else if(args[1].equalsIgnoreCase("addbronzespawn")) {
								Location loc = p.getLocation();
								map.addBronzeSpawn(loc);
								
								p.sendMessage(ChatColor.GREEN + "Added Bronze Spawner");
							} else if(args[1].equalsIgnoreCase("addsilverspawn")) {
								Location loc = p.getLocation();
								map.addSilverSpawn(loc);
								
								p.sendMessage(ChatColor.GREEN + "Added Silver Spawner");
							} else if(args[1].equalsIgnoreCase("addgoldspawn")) {
								Location loc = p.getLocation();
								map.addGoldSpawn(loc);
								
								p.sendMessage(ChatColor.GREEN + "Added Gold Spawner");
							} else if(args[1].equalsIgnoreCase("team")) {
								if(args.length > 3) {
									if(args[2].equalsIgnoreCase("add")) {
										if(args.length != 6) {
											p.sendMessage("/bedwars team add <name> <display> <maxplayers>");
											return false;
										}
										
										String name = args[3];
										String display = args[4];
										
										int maxplayers = 0;
										try {
											maxplayers = Integer.parseInt(args[5]);
										} catch(NumberFormatException e) {
											p.sendMessage("/bedwars team add <name> <display> <maxplayers>");
											return false;
										}
										
										BedwarsTeam team = new BedwarsTeam(name, display, maxplayers, null, null);
										map.addTeam(team);
										p.sendMessage(ChatColor.DARK_GREEN + "Added team " + name);
									} else if(args[2].equalsIgnoreCase("remove")) {
										if(args.length != 4) {
											p.sendMessage("/bedwars team remove <name>");
											return false;
										}
										
										for(BedwarsTeam team : map.getTeams()) {
											if(team.getName().equalsIgnoreCase(args[3])) {
												map.getTeams().remove(team);
												p.sendMessage(ChatColor.DARK_GREEN + "Removed team " + team.getName());
												return true;
											}
										}
									} else if(args[2].equalsIgnoreCase("setBed")) {
										if(args.length != 4) {
											p.sendMessage("/bedwars team setBed <name>");
											return false;
										}
										
										Location loc = p.getLocation();
										
										for(BedwarsTeam team : map.getTeams()) {
											if(team.getName().equalsIgnoreCase(args[3])) {
												team.setBedSpawn(loc);
												p.sendMessage(ChatColor.DARK_GREEN + "Set bed spawn for " + team.getName());
												return true;
											}
										}
										
									} else if(args[2].equalsIgnoreCase("setSpawn")) {
										if(args.length != 4) {
											p.sendMessage("/bedwars team setSpawn <name>");
											return false;
										}
										
										Location loc = p.getLocation();
										
										for(BedwarsTeam team : map.getTeams()) {
											if(team.getName().equalsIgnoreCase(args[3])) {
												team.setTeamSpawn(loc);
												p.sendMessage(ChatColor.DARK_GREEN + "Set bed spawn for " + team.getName());
												return true;
											}
										}
									}
								} else {
									this.sendCommandHelp(p);
								}
							}
						} else if(args[1].equalsIgnoreCase("info")) {
							p.sendMessage(map.getMapName() + "\n" + map.getPath() + "\n" + map.getAuthor());
							for(BedwarsTeam team : map.getTeams()) {
								p.sendMessage(team.getName() + " - " + team.getDisplayName());
							}
						} else {
							this.sendCommandHelp(p);
						}
					}
				} else {
					this.sendCommandHelp(p);
				}
			} else {
				cs.sendMessage(ChatColor.RED + "Commands are only ingame allowed");
			}
		} else {
			cs.sendMessage(ChatColor.RED + "You don't have permission to execute this command.");
		}
		return false;
	}

	private void sendCommandHelp(Player p) {
		p.sendMessage("/bedwars createmap <worldname> <mapname>\n"
				+ "/bedwars savemap\n"
				+ "/bedwars map author <author>\n"
				+ "/bedwars map addvillager\n"
				+ "/bedwars map addbronzespawn\n"
				+ "/bedwars map addsilverspawn\n"
				+ "/bedwars map addgoldspawn\n"
				+ "/bedwars map team add <name> <display> <maxplayers>\n"
				+ "/bedwars map team remove <name>\n"
				+ "/bedwars map team setBed <name>\n"
				+ "/bedwars map team setSpawn <name>");
	}

}
