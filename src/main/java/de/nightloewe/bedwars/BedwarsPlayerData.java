package de.nightloewe.bedwars;

import org.bukkit.entity.Player;

import de.maltesermailo.api.minigame.PlayerData;

public class BedwarsPlayerData implements PlayerData {
	
	private BedwarsTeam team;
	
	private int coins = 0;
	
	private Player lastPlayerHit;
	private long lastPlayerHitTimestamp;
	
	public BedwarsTeam getTeam() {
		return team;
	}
	
	public void setTeam(BedwarsTeam team) {
		this.team = team;
	}

	public Player getLastPlayerHit() {
		return lastPlayerHit;
	}

	public void setLastPlayerHit(Player lastPlayerHit) {
		this.lastPlayerHit = lastPlayerHit;
	}

	public long getLastPlayerHitTimestamp() {
		return lastPlayerHitTimestamp;
	}

	public void setLastPlayerHitTimestamp(long lastPlayerHitTimestamp) {
		this.lastPlayerHitTimestamp = lastPlayerHitTimestamp;
	}
	
	public int getCoins() {
		return coins;
	}
	
	public void addCoins(int coins) {
		this.coins = this.coins + coins;
	}
	
}
