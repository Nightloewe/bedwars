package de.nightloewe.bedwars.util;

import org.bukkit.Bukkit;

/**
 * Shortcut to get the NMS Classes faster.
 */
public enum CloudPackage {
    MINECRAFT {
        @Override
        public String toString() {
            return "net.minecraft.server."
                    + Bukkit.getServer().getClass().getPackage().getName()
                    .substring(23, 30);
        }
    },
    CRAFTBUKKIT {
        @Override
        public String toString() {
            return Bukkit.getServer().getClass().getPackage().getName();
        }
    },
    PACKET_IN {
        @Override
        public String toString() {
            return "net.minecraft.server."
                    + Bukkit.getServer().getClass().getPackage().getName()
                    .substring(23, 30) + ".PacketPlayIn";
        }

        ;
    },
    PACKET_OUT {
        @Override
        public String toString() {
            return "net.minecraft.server."
                    + Bukkit.getServer().getClass().getPackage().getName()
                    .substring(23, 30) + ".PacketPlayOut";
        }

        ;
    };

    /**
     * Returns the Class with its full packagename given by the Enum which has been used.
     *
     * @param name - The Name of the Package e.g. "inventory.CraftInventory" or "NBTTagList"
     * @return The Class of the Package
     * @throws ClassNotFoundException - If the Class cannot be found
     */
    public Class<?> getDynClass(String name) throws ClassNotFoundException {
        return Class.forName(this.toString() + "." + name);
    }

}