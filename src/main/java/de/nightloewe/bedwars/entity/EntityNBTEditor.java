package de.nightloewe.bedwars.entity;

import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;

import de.nightloewe.bedwars.Bedwars;
import de.nightloewe.bedwars.util.CloudPackage;

import java.lang.reflect.InvocationTargetException;

/**
 * Supports Methods to manipulate Entities via Reflection.
 */
public class EntityNBTEditor {

    private static Class<?> nmsEntity;
    private static Class<?> craftEntity;
    private static Class<?> entityCreature;
    private static Class<?> entityVillager;
    private static Class<?> world;
    private static Class<?> craftWorld;

    static {
        try {
            nmsEntity = CloudPackage.MINECRAFT.getDynClass("Entity");
            craftEntity = CloudPackage.CRAFTBUKKIT.getDynClass("entity.CraftEntity");
            entityCreature = CloudPackage.MINECRAFT.getDynClass("EntityCreature");
            entityVillager = CloudPackage.MINECRAFT.getDynClass("EntityVillager");
            world = CloudPackage.MINECRAFT.getDynClass("World");
            craftWorld = CloudPackage.CRAFTBUKKIT.getDynClass("CraftWorld");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Spawns a Villager which rides on an ArmorStand.
     *
     * @param name - The Name of the Villager.
     * @param loc  - Where to spawn.
     */
    public static Villager spawnImmobileVillager(String name, Location loc) {
        try {
            ArmorStand vehicle = generateArmorStandVehicle(loc);
            Object nmsWorld = craftWorld.getMethod("getHandle").invoke(loc.getWorld());
            Object villagerEntity = entityVillager.getConstructor(world).newInstance(nmsWorld);
            entityVillager.getMethod("setPositionRotation", double.class, double.class, double.class, float.class, float.class)
                    .invoke(villagerEntity, loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), 0.0F);
            Villager villager = (Villager) entityVillager.getMethod("getBukkitEntity").invoke(villagerEntity);
            villager.setCustomName(name.replace("&", "§"));
            vehicle.setPassenger(villager);
            world.getMethod("addEntity", nmsEntity).invoke(nmsWorld, villagerEntity);
            Bedwars.instance().getLogger().info("Spawned Villager. Coord:" + loc.toString());
            return villager;
        } catch (InstantiationException | IllegalAccessException
                | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void fixHeadRotation(Object standNmsEntity, Location loc) {
        try {
            //Hotfix for the Rotate Head Bug:
            entityVillager.getMethod("h", float.class).invoke(standNmsEntity, loc.getYaw());
            entityVillager.getMethod("i", float.class).invoke(standNmsEntity, loc.getYaw());
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }


    /**
     * Creates an Armor Stand which has the noAI and noGravity Tag to
     * let its riders stand still and be quiet.
     *
     * @param loc - The Location where the ArmorStand should spawn.
     * @return The ArmorStand to generate.
     */
    private static ArmorStand generateArmorStandVehicle(Location loc) {
        if (loc == null)
            Bedwars.instance().getLogger().info("Loc == null");
        if (loc.getWorld() == null)
            Bedwars.instance().getLogger().info("World == null");
        //1.7775D gained by checking.
        Location locn = loc.subtract(0, 1.3775D, 0);
        ArmorStand stand = (ArmorStand) loc.getWorld().spawnEntity(locn, EntityType.ARMOR_STAND);
        stand.setVisible(false);
        stand.setGravity(false);
        stand.setCustomNameVisible(false);
        return stand;
    }

}
