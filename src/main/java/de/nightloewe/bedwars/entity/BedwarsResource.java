package de.nightloewe.bedwars.entity;

import org.bukkit.Material;

public enum BedwarsResource {

	GOLD("&6Gold", Material.GOLD_INGOT),
	SILVER("&7Eisen", Material.IRON_INGOT),
	BRONZE("&cBronze", Material.CLAY_BRICK);
	
	private String name;
	private Material mat;
	
	private BedwarsResource(String name, Material mat) {
		this.name = name;
		this.mat = mat;
	}
	
	public String getName() {
		return name;
	}
	
	public Material getMaterial() {
		return mat;
	}
}
