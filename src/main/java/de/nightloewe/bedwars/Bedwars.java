package de.nightloewe.bedwars;

import java.io.File;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

import de.maltesermailo.api.minigame.AbstractMinigame;
import de.maltesermailo.api.minigame.Map;
import de.maltesermailo.api.minigame.MapFactory;
import de.maltesermailo.api.minigame.PlayerHandle;
import de.maltesermailo.impl.minigame.gamerules.SharedGameRules;
import de.nightloewe.bedwars.commands.BedwarsCommand;
import de.nightloewe.bedwars.gamephases.BedwarsIngameGamePhase;
import de.nightloewe.bedwars.gamephases.BedwarsLobbyGamePhase;
import de.nightloewe.bedwars.gamephases.listener.JoinListener;
import de.nightloewe.bedwars.gamephases.listener.LegendaryNickListener;
import de.nightloewe.bedwars.gamephases.listener.LegendarySelfColorListener;
import de.nightloewe.bedwars.inventory.BedwarsShop;
import net.md_5.bungee.api.ChatColor;

public class Bedwars extends AbstractMinigame {

	private static Bedwars instance;
	
	private HashMap<BedwarsTeam, BedwarsShop> shops = new HashMap<BedwarsTeam, BedwarsShop>();
	
	public static Bedwars instance() {
		return Bedwars.instance;
	}
	
	@Override
	public void onSecondTick(int remaining) {
		if(remaining == 60) {
			this.broadcastMessage("Das Spiel startet in &e" + remaining + " &7Sekunden.");
		} else if(remaining == 30) {
			this.broadcastMessage("Das Spiel startet in &e" + remaining + " &7Sekunden.");
		} else if(remaining == 15) {
			this.broadcastMessage("Das Spiel startet in &e" + remaining + " &7Sekunden.");
		} else if(remaining == 10) {
			this.broadcastMessage("Das Spiel startet in &e" + remaining + " &7Sekunden.");
			this.broadcastMessage("Map: &e" + this.context().mapFactory().current().getMapName());
			Bukkit.getScheduler().runTaskAsynchronously(this, new Runnable() {

				@Override
				public void run() {
					long t1 = System.currentTimeMillis();
					Bedwars.this.context().mapFactory().copyWorld(Bedwars.this.context().mapFactory().current());
					long t2 = System.currentTimeMillis();
					Bukkit.broadcast(ChatColor.translateAlternateColorCodes('&', "&bBedwars &7\\u00BB &cDebug: Loaded map after " + String.valueOf(t2-t1) + "ms"), "minigame.admin");
				}
				
			});
		} else if(remaining == 0) {
			return;
		} else if(remaining < 10) {
			this.broadcastMessage("Das Spiel startet in &e" + remaining + " &7Sekunden.");
		}
	}

	@Override
	public void load() {
	}

	@Override
	public void enable() {
		Bedwars.instance = this;
		this.context().miniGameController().setMaxPlayers(0);
		if(this.context().cloudController() != null) {
			this.context().cloudController().setMotd("BEDWARS");
			this.context().cloudController().setMap("Bedwars");
		}
		
		this.getConfig().addDefault("tasks.bronzeSpawnPeriod", 20L);
		this.getConfig().addDefault("tasks.silverSpawnPeriod", 20L * 10L);
		this.getConfig().addDefault("tasks.goldSpawnPeriod", 20L * 30L);
		this.getConfig().options().copyDefaults(true);
		this.saveConfig();
		
		Bukkit.getPluginCommand("bedwars").setExecutor(new BedwarsCommand());
		
		this.context().miniGameController().getGamePhaseManager().registerGamePhase("Lobby", new BedwarsLobbyGamePhase(this));
		this.context().miniGameController().getGamePhaseManager().registerGamePhase("Ingame", new BedwarsIngameGamePhase(this));
		
		this.getDataFolder().mkdir();
		
		File mapFolder = new File(this.getDataFolder(), "maps");
		mapFolder.mkdir();
		
		Bukkit.getLogger().info("Loading maps from: " + mapFolder.getAbsolutePath());
		
		//Initializing mapFactory
		//Setting map Directory and class type and loading maps from map directory
		MapFactory factory = this.context().mapFactory();
		factory.setMapDirectory(mapFolder.getAbsolutePath());
		factory.setMapClassType(BedwarsMap.class);
		factory.loadMaps();
		
		Bukkit.getLogger().info("MapPool size: " + factory.getMaps().size());
		
		if(factory.mapsExisting()) {
			//Getting random map from map pool
			this.context().mapFactory().setCurrentMap((BedwarsMap) factory.getRandomMap());
			
			//Getting current map
			BedwarsMap map = ((BedwarsMap) this.context().mapFactory().current());
			
			//Setting cloud information
			String mapCount = map.getTeams().size() + "x" + map.getTeams().get(0).getMaxPlayers();
			
			this.context().cloudController().setMap(map.getMapName() + " (" + mapCount + ")");
			
			//Start gamePhaseManager and call lobby phase
			this.context().miniGameController().getGamePhaseManager().setRunning(true);
			this.context().miniGameController().getGamePhaseManager().callGamePhase(this.context().miniGameController().getGamePhaseManager().getGamePhase("Lobby"));
		} else {
			this.context().miniGameController().getGamePhaseManager().setRunning(false);
			this.context().miniGameController().getGameRule(SharedGameRules.World.SETUP_MODE).enable();
		}
	}

	@Override
	public void onMapChange(Map map) {
		Bukkit.getScheduler().runTaskAsynchronously(this, new Runnable() {

			@Override
			public void run() {
				Bedwars.this.context().mapFactory().copyWorld(map);
			}
			
		});
		
		BedwarsMap bedwarsMap = (BedwarsMap) map;
		
		String mapCount = bedwarsMap.getTeams().size() + "x" + bedwarsMap.getTeams().get(0).getMaxPlayers();
		
		this.context().cloudController().setMap(map.getMapName() + " (" + mapCount + ")");
		
		for(Player p : Bukkit.getOnlinePlayers()) {
			PlayerHandle handle = this.context().miniGameController().getPlayerHandle(p);
			handle.getCurrentScoreboard().setScore(3, "&c" 
					+ this.context().mapFactory().current().getMapName() 
					+ " &7(&e" + ((BedwarsMap) this.context().mapFactory().current()).getTeams().size() 
					+ "&7x&e" 
					+ ((BedwarsMap) this.context().mapFactory().current()).getTeams().get(0).getMaxPlayers() 
					+ "&7) ", 2);
		}
	}
	
	public void broadcastMessage(String message) {
		Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&bBedwars &7\u00BB " + message));
	}
	
	public void sendMessage(Player p, String message) {
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&bBedwars &7\u00BB " + message));
	}
	
	public String getPrefix() {
		return "&bBedwars &7\u00BB ";
	}
	
	public HashMap<BedwarsTeam, BedwarsShop> getShops() {
		return shops;
	}

}
