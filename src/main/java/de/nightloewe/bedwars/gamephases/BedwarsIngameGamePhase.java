package de.nightloewe.bedwars.gamephases;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Predicate;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitTask;

import de.maltesermailo.api.Context;
import de.maltesermailo.api.MinigameController;
import de.maltesermailo.api.minigame.AbstractGamePhase;
import de.maltesermailo.api.minigame.AbstractMinigame;
import de.maltesermailo.api.minigame.GamePhase;
import de.maltesermailo.api.minigame.ManagedScoreboard;
import de.maltesermailo.api.minigame.PlayerHandle;
import de.maltesermailo.api.minigame.PlayerScoreboard;
import de.maltesermailo.api.utils.ItemFactory;
import de.maltesermailo.impl.minigame.MapLocation;
import de.maltesermailo.impl.minigame.gamerules.SharedGameRules;
import de.nightloewe.bedwars.Bedwars;
import de.nightloewe.bedwars.BedwarsMap;
import de.nightloewe.bedwars.BedwarsPlayerData;
import de.nightloewe.bedwars.BedwarsTeam;
import de.nightloewe.bedwars.entity.BedwarsResource;
import de.nightloewe.bedwars.entity.EntityNBTEditor;
import de.nightloewe.bedwars.gamephases.listener.BlockListeners;
import de.nightloewe.bedwars.gamephases.listener.DamageListener;
import de.nightloewe.bedwars.gamephases.listener.EntityDamageByEntityListener;
import de.nightloewe.bedwars.gamephases.listener.PlayerDeathListener;
import de.nightloewe.bedwars.gamephases.listener.PlayerInteractEntityListener;
import de.nightloewe.bedwars.gamephases.listener.PlayerPickupItemListener;
import de.nightloewe.bedwars.gamephases.listener.PlayerQuitListener;
import de.nightloewe.bedwars.gamephases.listener.PlayerRespawnListener;
import de.nightloewe.bedwars.inventory.BedwarsShop;
import net.md_5.bungee.api.ChatColor;

public class BedwarsIngameGamePhase extends AbstractGamePhase {

	private Context ctx;
	private BedwarsMap map;
	private BukkitTask bronzeTask;
	private BukkitTask silverTask;
	private BukkitTask goldTask;
	
	public BedwarsIngameGamePhase(AbstractMinigame minigame) {
		this.ctx = minigame.context();
	}
	
	@Override
	public String getName() {
		return "Ingame";
	}

	@Override
	public GamePhase next() {
		this.bronzeTask.cancel();
		this.bronzeTask = null;
		
		this.silverTask.cancel();
		this.silverTask = null;
		
		this.goldTask.cancel();
		this.goldTask = null;
		return null;
	}

	@Override
	public Predicate<Context> whenChange() {
		return v -> false;
	}

	@Override
	public void init() {
		if(this.ctx.cloudController() != null) {
			this.ctx.cloudController().setStatus("ingame");
		}
		this.orderTeams();
		
		this.map = (BedwarsMap) this.ctx.mapFactory().current();
		this.ctx.mapFactory().loadMap(this.map);
		
		for(Player p : Bukkit.getOnlinePlayers()) {
			p.getInventory().clear();
			p.setGameMode(GameMode.SURVIVAL);
			
			PlayerHandle handle = this.ctx.miniGameController().getPlayerHandle(p);
			BedwarsPlayerData data = handle.getPlayerData();
			BedwarsTeam team = data.getTeam();
			
			p.setDisplayName(team.getDisplayName().substring(0,2) + p.getDisplayName());
			
			PlayerScoreboard sb = new PlayerScoreboard(p.getName(), "Bedwars");
			for(int i = 0; i < this.map.getTeams().size(); i++) {
				BedwarsTeam t = this.map.getTeams().get(i);
				sb.setScore(i, "&2✔ " + t.getDisplayName(), t.getTeamSize());
			}
			handle.setCurrentScoreboard(sb);
			
			p.teleport(team.getTeamSpawn());
		}
		
		for(MapLocation villagerLoc : this.map.getVillagers()) {
			EntityNBTEditor.spawnImmobileVillager("", villagerLoc.get());
		}
		
		this.registerTasks();
		this.registerShops();
		this.registerListeners();
		this.applyGameRules();
	}
	
	private void registerListeners() {
		this.ctx.eventController().addListener(new DamageListener(this.ctx));
		this.ctx.eventController().addListener(new PlayerDeathListener(this.ctx));
		this.ctx.eventController().addListener(new PlayerInteractEntityListener(this.ctx));
		this.ctx.eventController().addListener(new BlockListeners(this.ctx));
		this.ctx.eventController().addListener(new PlayerRespawnListener(this.ctx));
		this.ctx.eventController().addListener(new PlayerQuitListener(this.ctx));
		this.ctx.eventController().addListener(new EntityDamageByEntityListener(this.ctx));
		//this.ctx.eventController().addListener(new PlayerPickupItemListener());
	}

	private void registerShops() {
		for(BedwarsTeam team : this.map.getTeams()) {
			Bedwars.instance().getShops().put(team, new BedwarsShop(team));
		}
	}

	private void registerTasks() {
		this.bronzeTask = Bukkit.getScheduler().runTaskTimer(this.ctx.getPlugin(), new Runnable() {

			@Override
			public void run() {
				ItemStack item = ItemFactory.newFactory(BedwarsResource.BRONZE.getMaterial(), BedwarsResource.BRONZE.getName()).build();
				
				for(MapLocation loc : BedwarsIngameGamePhase.this.map.getBronzeSpawns()) {
					loc.get().getWorld().dropItemNaturally(loc.get(), item);
				}
			}
			
		}, this.ctx.getPlugin().getConfig().getLong("tasks.bronzeSpawnPeriod"), this.ctx.getPlugin().getConfig().getLong("tasks.bronzeSpawnPeriod"));
		
		this.silverTask = Bukkit.getScheduler().runTaskTimer(this.ctx.getPlugin(), new Runnable() {

			@Override
			public void run() {
				ItemStack item = ItemFactory.newFactory(BedwarsResource.SILVER.getMaterial(), BedwarsResource.SILVER.getName()).build();
				
				for(MapLocation loc : BedwarsIngameGamePhase.this.map.getSilverSpawns()) {
					loc.get().getWorld().dropItemNaturally(loc.get(), item);
				}
			}
			
		}, this.ctx.getPlugin().getConfig().getLong("tasks.silverSpawnPeriod"), this.ctx.getPlugin().getConfig().getLong("tasks.silverSpawnPeriod"));
		
		this.goldTask = Bukkit.getScheduler().runTaskTimer(this.ctx.getPlugin(), new Runnable() {

			@Override
			public void run() {
				ItemStack item = ItemFactory.newFactory(BedwarsResource.GOLD.getMaterial(), BedwarsResource.GOLD.getName()).build();
				
				for(MapLocation loc : BedwarsIngameGamePhase.this.map.getGoldSpawns()) {
					loc.get().getWorld().dropItemNaturally(loc.get(), item);
				}
			}
			
		}, this.ctx.getPlugin().getConfig().getLong("tasks.goldSpawnPeriod"), this.ctx.getPlugin().getConfig().getLong("tasks.goldSpawnPeriod"));
	}
	
	private void applyGameRules() {
		MinigameController mController = this.ctx.miniGameController();
		
		mController.getGameRule(SharedGameRules.World.NO_RAIN.name()).enable();
		mController.getGameRule(SharedGameRules.World.NO_TIME_CHANGE.name()).enable();
		mController.getGameRule(SharedGameRules.Chat.JOIN_MESSAGE).disable();
		mController.getGameRule(SharedGameRules.Chat.QUIT_MESSAGE).disable();
		mController.getGameRule(SharedGameRules.Block.BLOCK_BREAK.name()).disable();
		mController.getGameRule(SharedGameRules.Block.BLOCK_PLACE.name()).disable();
		mController.getGameRule(SharedGameRules.Block.BLOCK_BURN.name()).disable();
		
		World world = this.map.getSpectatorSpawn().getWorld();
		world.setGameRuleValue("doFireTick", "false");
	}

	private void orderTeams() {
		BedwarsMap map = (BedwarsMap) this.ctx.mapFactory().current();
		
		List<PlayerHandle> playersWithoutTeam = map.getPlayersWithoutTeam();
		
		int teamsOverNull = 0;
		for(Entry<BedwarsTeam, Integer> entry : map.getTeamSizes().entrySet()) {
			int size = entry.getValue();
			
			if(size > 0) {
				teamsOverNull++;
			}
		}
		
		if(teamsOverNull == 1 && playersWithoutTeam.size() == 0) {
			for(Player p : Bukkit.getOnlinePlayers()) {
				PlayerHandle handle = this.ctx.miniGameController().getPlayerHandle(p);
				BedwarsPlayerData data = handle.getPlayerData();
				
				BedwarsTeam team = map.getRandomTeam();
				
				if(team == null) {
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cDu konntest keinem Team zugewiesen werden."));
					p.kickPlayer("");
					continue;
				}
				
				data.setTeam(team);
				handle.refreshNick();
			}
			return;
		}
		
		if(playersWithoutTeam.size() > 0) {
			for(PlayerHandle handle : playersWithoutTeam) {
				HashMap<BedwarsTeam, Integer> sizes = map.getTeamSizes();
				BedwarsTeam team = null;
				int size = 500;
				for(Entry<BedwarsTeam, Integer> entry : sizes.entrySet()) {
					if(entry.getValue() < size) {
						size = entry.getValue();
						team = entry.getKey();
					}
				}
				
				if(team == null) {
					team = map.getRandomTeam();
					if(team == null) {
						handle.getPlayer().kickPlayer("&cDu konntest keinem Team zugewiesen werden.");
						return;
					}
				}
				
				BedwarsPlayerData data = handle.getPlayerData();
				data.setTeam(team);
				handle.refreshNick();
			}
		}
	}
}
