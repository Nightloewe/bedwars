package de.nightloewe.bedwars.gamephases.listener;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

import de.maltesermailo.api.Context;
import de.nightloewe.bedwars.gamephases.BedwarsIngameGamePhase;

public class DamageListener implements Listener {

	public Context ctx;
	
	public DamageListener(Context ctx) {
		this.ctx = ctx;
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		if(this.ctx.miniGameController().getGamePhaseManager().current() instanceof BedwarsIngameGamePhase) {
			if(e.getEntityType() != EntityType.PLAYER) {
				e.setCancelled(true);
			}
		}
	}
}
