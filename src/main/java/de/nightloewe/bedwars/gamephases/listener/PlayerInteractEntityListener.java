package de.nightloewe.bedwars.gamephases.listener;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import de.maltesermailo.api.Context;
import de.maltesermailo.api.minigame.PlayerHandle;
import de.nightloewe.bedwars.Bedwars;
import de.nightloewe.bedwars.BedwarsPlayerData;
import de.nightloewe.bedwars.BedwarsTeam;
import de.nightloewe.bedwars.inventory.BedwarsShop;

public class PlayerInteractEntityListener implements Listener {

	private Context ctx;
	
	public PlayerInteractEntityListener(Context ctx) {
		this.ctx = ctx;
	}
	
	@EventHandler
	public void onEntityInteract(PlayerInteractEntityEvent e) {
		if(e.getRightClicked().getType() == EntityType.VILLAGER) {
			e.setCancelled(true);
			Player p = e.getPlayer();
			PlayerHandle handle = this.ctx.miniGameController().getPlayerHandle(p);
			
			if(handle.isSpectator()) {
				return;
			}
			
			BedwarsPlayerData data = handle.getPlayerData();
			BedwarsTeam team = data.getTeam();
			
			BedwarsShop shop = Bedwars.instance().getShops().get(team);
			shop.openInventory(p);
		}
	}
}
