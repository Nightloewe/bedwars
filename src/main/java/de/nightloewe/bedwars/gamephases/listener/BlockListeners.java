package de.nightloewe.bedwars.gamephases.listener;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.metadata.FixedMetadataValue;

import de.maltesermailo.api.Context;
import de.maltesermailo.api.minigame.ManagedScoreboard;
import de.maltesermailo.api.minigame.PlayerHandle;
import de.maltesermailo.api.minigame.PlayerScoreboard;
import de.maltesermailo.api.utils.ItemFactory;
import de.nightloewe.bedwars.Bedwars;
import de.nightloewe.bedwars.BedwarsMap;
import de.nightloewe.bedwars.BedwarsPlayerData;
import de.nightloewe.bedwars.BedwarsTeam;
import net.md_5.bungee.api.ChatColor;

public class BlockListeners implements Listener {

	private Context ctx;

	public BlockListeners(Context ctx) {
		this.ctx = ctx;
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onBlockPlace(BlockPlaceEvent e) {
		Block block = e.getBlock();
		block.setMetadata("placed", new FixedMetadataValue(this.ctx.getPlugin(), true));
		e.setCancelled(false);
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onBlockBreak(BlockBreakEvent e) {
		Block block = e.getBlock();
		
		if(block.hasMetadata("placed")) {
			e.setCancelled(false);
			return;
		}
		
		if(block.getType() == Material.BED_BLOCK) {
			Player p = (Player) e.getPlayer();
			PlayerHandle handle = this.ctx.miniGameController().getPlayerHandle(p);
			BedwarsPlayerData data = handle.getPlayerData();
			
			BedwarsMap map = (BedwarsMap) this.ctx.mapFactory().current();
			for(BedwarsTeam team : map.getTeams()) {
				if(map.equalBedLocation(block.getLocation(), team.getBedSpawn())) {
					if(team == data.getTeam()) {
						Bedwars.instance().sendMessage(p, "&cDu kannst dein eigenes Bett nicht zerstören!");
						e.setCancelled(true);
						return;
					} else {
						e.getBlock().breakNaturally();
						for(Player all : Bukkit.getOnlinePlayers()) {
							all.playSound(all.getLocation() , Sound.WITHER_DEATH , (float) 0.5, (float) 1.0);
							PlayerHandle allHandle = this.ctx.miniGameController().getPlayerHandle(all);
							PlayerScoreboard sb = allHandle.getCurrentScoreboard();
							for(int i = 0; i < map.getTeams().size(); i++) {
								BedwarsTeam t = map.getTeams().get(i);
								if(!t.hasBed()) {
									sb.removeScore(i);
									sb.setScore(i, "&4✕ " + t.getDisplayName(), t.getTeamSize());
								}
							}
						}
						Bedwars.instance().broadcastMessage("&4Das Bett von Team " + team.getDisplayName() + " &4wurde zerstört!");
						data.addCoins(50);
						return;
					}
				}
			}
		}
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onDrop(ItemSpawnEvent e) {
		Item item = e.getEntity();
		if(item.getItemStack().getType() == Material.BED) {
			item.remove();
		} else if(item.getItemStack().getType() == Material.SANDSTONE) {
			item.setItemStack(ItemFactory.newFactory(Material.SANDSTONE, "Sandstein").build());
		} else if(item.getItemStack().getType() == Material.ENDER_STONE) {
			ItemFactory.newFactory(Material.ENDER_STONE, "Endstein").lore("Steinblock aus der Endwelt. ", "Wesentlich robuster als Sandstein.").build();
		} else if(item.getItemStack().getType() == Material.IRON_BLOCK) {
			ItemFactory.newFactory(Material.IRON_BLOCK, "Eisenblock").build();
		} else if(item.getItemStack().getType() == Material.GLOWSTONE) {
			ItemFactory.newFactory(Material.GLOWSTONE, "Glowstone").build();
		} else if(item.getItemStack().getType() == Material.GLASS) {
			ItemFactory.newFactory(Material.GLASS, "Glas").build();
		}
			
	}
	
}
