package de.nightloewe.bedwars.gamephases.listener;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import de.maltesermailo.api.Context;
import de.nightloewe.bedwars.gamephases.BedwarsIngameGamePhase;

public class PlayerInteractListener implements Listener {

	private Context ctx;

	public PlayerInteractListener(Context ctx) {
		this.ctx = ctx;
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onInteract(PlayerInteractEvent e) {
		if(this.ctx.miniGameController().getGamePhaseManager().isRunning()
				&& this.ctx.miniGameController().getGamePhaseManager().current() instanceof BedwarsIngameGamePhase) {
			if(e.getClickedBlock() != null) {
				if(e.getClickedBlock().getType() == Material.WOOD_DOOR) {
					e.setCancelled(false);
				} else if(e.getClickedBlock().getType() == Material.TRAP_DOOR) {
					e.setCancelled(false);
				} else if(e.getClickedBlock().getType() == Material.WORKBENCH || e.getClickedBlock().getType() == Material.ANVIL || e.getClickedBlock().getType() == Material.ARMOR_STAND 
						|| e.getClickedBlock().getType() == Material.BREWING_STAND || e.getClickedBlock().getType() == Material.FENCE || e.getClickedBlock().getType() == Material.CAULDRON
						|| e.getClickedBlock().getType() == Material.DISPENSER || e.getClickedBlock().getType() == Material.DROPPER || e.getClickedBlock().getType() == Material.LEVER
						|| e.getClickedBlock().getType() == Material.STONE_BUTTON || e.getClickedBlock().getType() == Material.WOOD_BUTTON || e.getClickedBlock().getType().name().endsWith("PLATE")) {
					e.setCancelled(true);
				}
			}
		} else {
			e.setCancelled(true);
		}
	}
}
