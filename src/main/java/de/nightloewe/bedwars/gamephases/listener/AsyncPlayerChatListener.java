package de.nightloewe.bedwars.gamephases.listener;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import de.maltesermailo.api.Context;
import de.maltesermailo.api.minigame.PlayerHandle;
import de.nightloewe.bedwars.BedwarsPlayerData;
import de.nightloewe.bedwars.gamephases.BedwarsIngameGamePhase;

public class AsyncPlayerChatListener implements Listener {

	private Context ctx;
	
	public AsyncPlayerChatListener(Context ctx) {
		this.ctx = ctx;
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onChat(AsyncPlayerChatEvent e) {
		Player p = e.getPlayer();
		PlayerHandle handle = this.ctx.miniGameController().getPlayerHandle(p);
		BedwarsPlayerData data = handle.getPlayerData();
    	
        String nick = e.getPlayer().getDisplayName();
        if (nick == null || nick.isEmpty()) {
            nick = e.getPlayer().getName();
        }
		
		if(this.ctx.miniGameController().getGamePhaseManager().isRunning() && this.ctx.miniGameController().getGamePhaseManager().current() instanceof BedwarsIngameGamePhase) {
			if(data.getTeam() != null) {
				if(e.getMessage().startsWith("@all ") || e.getMessage().startsWith("@a ")) {
					String msg = "";
					if(e.getMessage().startsWith("@a ")) {
						msg = e.getMessage().substring(3, e.getMessage().length());
					} else {
						msg = e.getMessage().substring(5, e.getMessage().length());
					}
					e.setFormat(ChatColor.translateAlternateColorCodes('&', "&7@all: " + data.getTeam().getDisplayName().substring(0,2) + nick + "&r: ") + msg);
				} else {
					e.getRecipients().clear();
					for(Player all : Bukkit.getOnlinePlayers()) {
						PlayerHandle allHandle = this.ctx.miniGameController().getPlayerHandle(all);
						BedwarsPlayerData allData = allHandle.getPlayerData();
						
						if(!allHandle.isSpectator() && (allData.getTeam() != null && allData.getTeam() == data.getTeam())) {
							e.getRecipients().add(all);
							e.setFormat(ChatColor.translateAlternateColorCodes('&', data.getTeam().getDisplayName().substring(0,2) + nick + "&r: ") + e.getMessage());
						}
					}
				}
			} else {
				if(handle.isSpectator()) {
					e.getRecipients().clear();
					for(Player all : Bukkit.getOnlinePlayers()) {
						PlayerHandle allHandle = this.ctx.miniGameController().getPlayerHandle(all);
						if(allHandle.isSpectator()) {
							e.getRecipients().add(all);
						}
					}
					e.setFormat(ChatColor.translateAlternateColorCodes('&', "&7" + e.getPlayer().getName() + "&r: ") + e.getMessage());
				}
			}
		} else {
			if(data.getTeam() != null) {
				e.setFormat(ChatColor.translateAlternateColorCodes('&', data.getTeam().getDisplayName().substring(0,2) + nick + "&r: ") + e.getMessage());
			}
		}
	}
}
