package de.nightloewe.bedwars.inventory;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import de.maltesermailo.api.utils.ItemFactory;
import de.nightloewe.bedwars.Bedwars;
import de.nightloewe.bedwars.BedwarsTeam;
import de.nightloewe.bedwars.entity.BedwarsResource;
import net.md_5.bungee.api.ChatColor;

public class BedwarsShop implements Listener {

	private Inventory shopInventory;
	private List<BedwarsShopCategory> categories = new ArrayList<BedwarsShopCategory>();
	
	public BedwarsShop(BedwarsTeam team) {
		this.shopInventory = Bukkit.createInventory(null, 27, "Shop");
		
		//Glass item
		ItemStack glassitem = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7);
		ItemMeta glassmeta = glassitem.getItemMeta();
		
		glassmeta.setDisplayName(" ");
		
		glassitem.setItemMeta(glassmeta);
		
		for(int i = 0; i < 9; i++) {
			this.shopInventory.setItem(i, glassitem);
		}
		
		DyeColor dyeColor = TeamChooserItem.asWoolColor(team.getDisplayName().charAt(1));
		
		//0: Blocks
		BedwarsShopCategory blocks = new BedwarsShopCategory("Blöcke", ItemFactory.newFactory(Material.SANDSTONE).build());
		blocks.addLore("Hier findest du alles, was du zum bauen brauchst.");
		blocks.addItem("Sandstein", ItemFactory.newFactory(Material.SANDSTONE, "Sandstein").build(2), BedwarsResource.BRONZE, 1);
		blocks.addItem("Endstein", ItemFactory.newFactory(Material.ENDER_STONE, "Endstein").lore("Steinblock aus der Endwelt. ", "Wesentlich robuster als Sandstein.").build(), BedwarsResource.BRONZE, 7);
		blocks.addItem("Eisenblock", ItemFactory.newFactory(Material.IRON_BLOCK, "Eisenblock").build(), BedwarsResource.SILVER, 3);
		blocks.addItem("Glowstone", ItemFactory.newFactory(Material.GLOWSTONE, "Glowstone").build(), BedwarsResource.BRONZE, 15);
		blocks.addItem("Glas", ItemFactory.newFactory(Material.GLASS, "Glas").build(), BedwarsResource.BRONZE, 4);
		
		//1: Armor
		BedwarsShopCategory armor = new BedwarsShopCategory("Rüstung", ItemFactory.newFactory(Material.CHAINMAIL_CHESTPLATE).build());
		armor.addLore("Hier findest du Ausrüstung für den Kampf");
		
		//Leather
		armor.addItem("", ItemFactory.newFactory(Material.LEATHER_HELMET).leatherColor(dyeColor).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).enchant(Enchantment.DURABILITY, 1).build(), BedwarsResource.BRONZE, 1);
		armor.addItem("", ItemFactory.newFactory(Material.LEATHER_LEGGINGS).leatherColor(dyeColor).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).enchant(Enchantment.DURABILITY, 1).build(), BedwarsResource.BRONZE, 1);
		armor.addItem("", ItemFactory.newFactory(Material.LEATHER_BOOTS).leatherColor(dyeColor).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).enchant(Enchantment.DURABILITY, 1).build(), BedwarsResource.BRONZE, 1);
		
		//Chainmail Chestplates
		armor.addItem("", ItemFactory.newFactory(Material.CHAINMAIL_CHESTPLATE).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build(), BedwarsResource.SILVER, 1);
		armor.addItem("", ItemFactory.newFactory(Material.CHAINMAIL_CHESTPLATE).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2).build(), BedwarsResource.SILVER, 3);
		armor.addItem("", ItemFactory.newFactory(Material.CHAINMAIL_CHESTPLATE).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3).build(), BedwarsResource.SILVER, 7);
		
		//2: Pickaxes
		BedwarsShopCategory pickaxes = new BedwarsShopCategory("Spitzhacken", ItemFactory.newFactory(Material.STONE_PICKAXE).build());
		pickaxes.addLore("Hier findest du Spitzhacken für jeden Gebrauch");
		pickaxes.addItem("", ItemFactory.newFactory(Material.WOOD_PICKAXE).enchant(Enchantment.DURABILITY, 1).enchant(Enchantment.DIG_SPEED, 1).itemFlag(ItemFlag.HIDE_ENCHANTS).build(), BedwarsResource.BRONZE, 4);
		pickaxes.addItem("", ItemFactory.newFactory(Material.STONE_PICKAXE).enchant(Enchantment.DURABILITY, 1).enchant(Enchantment.DIG_SPEED, 1).itemFlag(ItemFlag.HIDE_ENCHANTS).build(), BedwarsResource.SILVER, 2);
		pickaxes.addItem("", ItemFactory.newFactory(Material.IRON_PICKAXE).enchant(Enchantment.DURABILITY, 1).enchant(Enchantment.DIG_SPEED, 3).itemFlag(ItemFlag.HIDE_ENCHANTS).build(), BedwarsResource.GOLD, 1);
		
		//3: Weapons
		BedwarsShopCategory weapons = new BedwarsShopCategory("Waffen", ItemFactory.newFactory(Material.GOLD_SWORD).build());
		weapons.addLore("Die besten Waffen, um deine im Kampf zu bezwingen");
		weapons.addItem("Knüppel", ItemFactory.newFactory(Material.STICK).enchant(Enchantment.KNOCKBACK, 1).itemFlag(ItemFlag.HIDE_ENCHANTS).build(), BedwarsResource.BRONZE, 8);
		weapons.addItem("Goldschwert I", ItemFactory.newFactory(Material.GOLD_SWORD).enchant(Enchantment.DURABILITY, 1).build(), BedwarsResource.SILVER, 1);
		weapons.addItem("Goldschwert II", ItemFactory.newFactory(Material.GOLD_SWORD).enchant(Enchantment.DURABILITY, 1).enchant(Enchantment.DAMAGE_ALL, 1).build(), BedwarsResource.SILVER, 3);
		weapons.addItem("Eisenschwert", ItemFactory.newFactory(Material.IRON_SWORD).enchant(Enchantment.DURABILITY, 1).enchant(Enchantment.DAMAGE_ALL, 1).enchant(Enchantment.KNOCKBACK, 1).build(), BedwarsResource.GOLD, 5);
		
		//4: Bows
		BedwarsShopCategory bows = new BedwarsShopCategory("Bögen", ItemFactory.newFactory(Material.BOW).build());
		bows.addItem("Bogen I", ItemFactory.newFactory(Material.BOW).enchant(Enchantment.ARROW_INFINITE, 1).build(), BedwarsResource.GOLD, 3);
		bows.addItem("Bogen II", ItemFactory.newFactory(Material.BOW).enchant(Enchantment.ARROW_INFINITE, 1).enchant(Enchantment.ARROW_DAMAGE, 1).build(), BedwarsResource.GOLD, 7);
		bows.addItem("Bogen III", ItemFactory.newFactory(Material.BOW).enchant(Enchantment.ARROW_INFINITE, 1).enchant(Enchantment.ARROW_DAMAGE, 1).enchant(Enchantment.ARROW_KNOCKBACK, 1).build(), BedwarsResource.GOLD, 13);
		bows.addItem("Pfeil", ItemFactory.newFactory(Material.ARROW).build(), BedwarsResource.GOLD, 1);
		
		//5: Food
		BedwarsShopCategory food = new BedwarsShopCategory("Essen", ItemFactory.newFactory(Material.COOKED_BEEF).build());
		food.addItem("", ItemFactory.newFactory(Material.APPLE).build(), BedwarsResource.BRONZE, 1);
		food.addItem("", ItemFactory.newFactory(Material.COOKED_BEEF).build(), BedwarsResource.BRONZE, 2);
		food.addItem("", ItemFactory.newFactory(Material.CAKE).build(), BedwarsResource.SILVER, 1);
		food.addItem("", ItemFactory.newFactory(Material.GOLDEN_APPLE).build(), BedwarsResource.GOLD, 2);
		
		//6: Chests
		BedwarsShopCategory chests = new BedwarsShopCategory("Kisten", ItemFactory.newFactory(Material.CHEST).build());
		chests.addItem("", ItemFactory.newFactory(Material.CHEST).build(), BedwarsResource.SILVER, 1);
		chests.addItem("Team Kiste", ItemFactory.newFactory(Material.ENDER_CHEST).build(), BedwarsResource.GOLD, 1);
		
		//7: Potions
		BedwarsShopCategory potions = new BedwarsShopCategory("Tränke", ItemFactory.newFactory(Material.POTION).build());
		potions.addItem("Heilung I", ItemFactory.newFactory(Material.POTION).potionEffect(new PotionEffect(PotionEffectType.HEAL, 0, 1)).potionType(PotionType.REGEN).build(), BedwarsResource.SILVER, 3);
		potions.addItem("Heilung II", ItemFactory.newFactory(Material.POTION).potionEffect(new PotionEffect(PotionEffectType.HEAL, 0, 2)).potionType(PotionType.REGEN).build(), BedwarsResource.SILVER, 5);
		potions.addItem("Geschwindigkeits-Trank", ItemFactory.newFactory(Material.POTION).potionEffect(new PotionEffect(PotionEffectType.SPEED, 3600, 1)).potionType(PotionType.SPEED).build(), BedwarsResource.SILVER, 8);
		potions.addItem("Stärke-Trank", ItemFactory.newFactory(Material.POTION).potionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 3600, 1)).potionType(PotionType.STRENGTH).build(), BedwarsResource.GOLD, 7);
		
		//8: Spezial
		BedwarsShopCategory special = new BedwarsShopCategory("Spezial", ItemFactory.newFactory(Material.TNT).build());
		special.addItem("", ItemFactory.newFactory(Material.LADDER).build(), BedwarsResource.BRONZE, 1);
		special.addItem("", ItemFactory.newFactory(Material.WEB).build(), BedwarsResource.BRONZE, 16);
		special.addItem("", ItemFactory.newFactory(Material.FISHING_ROD).build(), BedwarsResource.SILVER, 5);
		special.addItem("", ItemFactory.newFactory(Material.FLINT_AND_STEEL).build(), BedwarsResource.SILVER, 7);
		special.addItem("", ItemFactory.newFactory(Material.TNT).build(), BedwarsResource.GOLD, 3);
		special.addItem("&5Enderperle", ItemFactory.newFactory(Material.ENDER_PEARL).build(), BedwarsResource.GOLD, 13);
		
		this.categories.add(blocks.build());
		this.categories.add(armor.build());
		this.categories.add(pickaxes.build());
		this.categories.add(weapons.build());
		this.categories.add(bows.build());
		this.categories.add(food.build());
		this.categories.add(chests.build());
		this.categories.add(potions.build());
		this.categories.add(special.build());
		
		for(int i = 0; i < this.categories.size(); i++) {
			BedwarsShopCategory cat = this.categories.get(i);
			
			if(cat != null) {
				this.shopInventory.setItem(i+9, cat.getItem());
			}
		}
		
		for(int i = 18; i < 27; i++) {
			this.shopInventory.setItem(i, glassitem);
		}
		
		Bukkit.getPluginManager().registerEvents(this, Bedwars.instance());
	}
	
	public Inventory getShopInventory() {
		return shopInventory;
	}
	
	public void openInventory(Player p) {
		p.openInventory(this.getShopInventory());
	}
	
	@EventHandler
	public void onCategoryClick(InventoryClickEvent e) {
		if(e.getInventory().equals(this.shopInventory)) {
			if(e.getCurrentItem() != null 
					&& e.getCurrentItem().hasItemMeta()
					&& e.getCurrentItem().getType() != Material.GLASS) {
				for(BedwarsShopCategory cat : this.categories) {
					if(e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes('&', cat.getName()))) {
						cat.getTrade().openTrade((Player)e.getWhoClicked(), 0);
					}
				}
			}
			e.setCancelled(true);
		} else if(e.getInventory().getType() == InventoryType.MERCHANT) {
			for(BedwarsShopCategory cat : this.categories) {
				if(e.getInventory().getTitle().equals(ChatColor.translateAlternateColorCodes('&', cat.getName()))) {
					int position = cat.getTrade().getSize() - 1;
					cat.getTrade().openTrade((Player)e.getWhoClicked(), position);
				}
			}
		}
	}
	
}
