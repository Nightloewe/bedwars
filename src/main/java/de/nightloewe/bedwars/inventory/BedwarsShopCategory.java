package de.nightloewe.bedwars.inventory;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.maltesermailo.api.utils.ItemFactory;
import de.nightloewe.bedwars.entity.BedwarsResource;
import de.nightloewe.bedwars.entity.ForceMerchantTrade;
import net.md_5.bungee.api.ChatColor;

public class BedwarsShopCategory {

	private String name;
	
	private List<String> lore = new ArrayList<String>();
	private List<BedwarsShopItem> items = new ArrayList<BedwarsShopItem>();
	
	private ItemStack item;
	
	private ForceMerchantTrade trade;
	
	public BedwarsShopCategory(String name, ItemStack item) {
		this.name = name;
		this.item = item;
		ItemMeta meta = this.item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		this.item.setItemMeta(meta);
		
		this.trade = new ForceMerchantTrade(ChatColor.translateAlternateColorCodes('&', this.name));
	}
	
	public String getName() {
		return name;
	}
	
	public ItemStack getItem() {
		return item;
	}
	
	public List<String> getLore() {
		return lore;
	}
	
	public void addLore(String lore) {
		this.lore.add(lore);
	}
	
	public List<BedwarsShopItem> getItems() {
		return items;
	}
	
	public void addItem(String name, ItemStack item, BedwarsResource needed, int neededAmount) {
		BedwarsShopItem shopItem = new BedwarsShopItem(name, item, this, needed, neededAmount);
		this.items.add(shopItem);
	}
	
	public void removeItem(BedwarsShopItem item) {
		this.items.remove(item);
	}
	
	public void removeItem(int item) {
		this.items.remove(item);
	}
	
	public BedwarsShopCategory build() {
		for(BedwarsShopItem item : this.getItems()) {
			this.trade.addTrade(ItemFactory.newFactory(item.getNeededResource().getMaterial(), item.getNeededResource().getName()).build(item.getNeededAmount()), item.getItem());
		}
		
		return this;
	}
	
	public ForceMerchantTrade getTrade() {
		return this.trade;
	}
}
