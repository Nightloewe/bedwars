package de.nightloewe.bedwars.inventory;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.nightloewe.bedwars.entity.BedwarsResource;
import net.md_5.bungee.api.ChatColor;

public class BedwarsShopItem {

	private String name;
	private BedwarsShopCategory category;
	private ItemStack item;
	private BedwarsResource neededResource;
	private int amountNeededResource;
	
	public BedwarsShopItem(String name, ItemStack item, BedwarsShopCategory category, BedwarsResource res, int amount) {
		this.name = name;
		if(name.isEmpty()) {
			this.name = item.getItemMeta().getDisplayName();
		}
		this.item = item;
		ItemMeta meta = this.item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		this.item.setItemMeta(meta);
		this.category = category;
		this.neededResource = res;
		this.amountNeededResource = amount;
	}

	public String getName() {
		return name;
	}

	public BedwarsShopCategory getCategory() {
		return category;
	}

	public ItemStack getItem() {
		return item;
	}
	
	public BedwarsResource getNeededResource() {
		return neededResource;
	}
	
	public int getNeededAmount() {
		return amountNeededResource;
	}
	
}
